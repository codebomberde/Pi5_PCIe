# Pi5_PCIe
A crude reverse engineering of the Raspberry Pi 5's FPC PCIe connector &amp; potential breakout board.

In this bare-bones project I document a working PCIe pinout talk through my hardware tinkering to get the Raspberry Pi 5's PCIe connected to something. I had only seen Raspberry Pi's boards with their RP2040 and lots of parts on board. My professional experience with PCIe was that it either just goes or it really doesn't. I decided to try and make it do something for no reason other than the technical challenge.

To see what I did, check out my walkthrough of the process on YouTube: [Raspberry Pi 5 - Unofficial PCIe Success - Hacking the Pi5's PCIe into life](https://youtu.be/G5VOzO_ERTM).

**Note: This project is a reverse engineering - Please wait for/use the official Raspberry Pi documentation when using the PCIe FPC**
